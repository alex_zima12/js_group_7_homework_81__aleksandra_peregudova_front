import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Main from "./Main/Main";

const App = () => (
    <>
    <nav className="navbar navbar-dark bg-dark">
        <p className="text-light">URL Shortener</p>
    </nav>
       <Switch>
         <Route path="/" exact component={Main}/>
      </Switch>
</>
);

export default App;
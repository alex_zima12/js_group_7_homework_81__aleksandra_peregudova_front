import React, {useState} from 'react';
import {sendURL} from "../store/actions/contactsActions/actions";
import {useDispatch, useSelector} from "react-redux";

const Main = () => {
    const dispatch = useDispatch();
    const link = useSelector(state => state.link);
    const  error = useSelector(state => state.error);

    const [state, setState] = useState({
        originalUrl: "",
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const submitFormHandler = e => {
        e.preventDefault();
        const obj = {
            originalUrl: state.originalUrl,
        }
        dispatch(sendURL(obj));
    };

    let shortUrl;
    if (link.length > 0) {
        shortUrl = <a
            href={`http://localhost:8000/links/${link}`}
            target="_blank"
            rel="noreferrer"
        >http://localhost/{link}</a>
    }

    return (
        <div className="container">
            <form onSubmit={submitFormHandler} className="container">
                <div className="form-group mt-3">
                    <label htmlFor="exampleInputEmail1">Paste the URL to be shortened</label>
                    <input type="text"
                           className="form-control"
                           id="exampleInputEmail1"
                           aria-describedby="emailHelp"
                           placeholder="Enter link"
                           name="originalUrl"
                           onChange={inputChangeHandler}
                           required
                    />
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
            <div className="mt-3">
                <p>Your shortened URL</p>
                {shortUrl}
            </div>
            <p>{error}</p>
        </div>
    );
};

export default Main;
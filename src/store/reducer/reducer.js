import {
    SEND_URL_SUCCESS,
    SEND_URL_ERROR
} from "../actions/actionTypes";

const initialState = {
    error: null,
    link: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_URL_SUCCESS:
            return {...state, link: action.value};
        case SEND_URL_ERROR:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default reducer;
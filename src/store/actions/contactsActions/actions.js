import {
    SEND_URL_ERROR,
    SEND_URL_SUCCESS
} from "../actionTypes";

import axios from "../../../axiosApi";

export const redirectUrl = (url) => {
    return async (url) => {
      await axios.get("/links/" + url);
    };
};

const sendUrlError = error => {
    return {type: SEND_URL_ERROR, error}
};

const sendUrlSuccess = value => {
    return {type: SEND_URL_SUCCESS, value};
};

export const sendURL = linkData => {
    return async dispatch => {
        try {
            await axios.post("/links", linkData).then((response) => {
            dispatch(sendUrlSuccess(response.data));
        });
        } catch (e) {
            console.log(e);
            dispatch(sendUrlError(e.message))
        }
    };
};